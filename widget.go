package templates

import (
	"fmt"
	"html/template"
	"strings"
	"sync"
)

// RegisterWidget registers a named template widget.
// The widget namespace is global to all templates.
// NewWidget should return a new instance of the widget, ready for use.
func RegisterWidget(name string, newWidget func() any) {
	widgetLock.Lock()
	defer widgetLock.Unlock()
	widgets[name] = newWidget
}

var widgets = make(map[string]func() any)
var widgetLock sync.Mutex

// A Widget is some type created inside templates for display purposes.
// The goal here is explicitly not to put more logic into templates,
// but to allow encapsulation and reuse of bits of UI
// that are hard to capture with named templates alone.
// Packages can register widgets using RegisterWidget.
func Widget(name string) (any, error) {
	widgetLock.Lock()
	newWidget := widgets[name]
	widgetLock.Unlock()
	if newWidget == nil {
		return nil, fmt.Errorf("templates: no widget named %s", name)
	}
	return newWidget(), nil
}

// Inline parses and renders the template text with the given data into a string.
// The returned string is template.HTML so it isn't over-escaped when used within a template.
//
// Inline is intended for use with widgets to allow
// insertion of template fragments at the point of need.
// Parsed templates are cached to improve performance.
// Inline templates are not associated with any other templates,
// so they cannot use {{ template "name" }} to include outside templates.
func Inline(data any, text string) (template.HTML, error) {
	inlineCacheLock.Lock()
	t := inlineCache[text] // Should we hash to get a shorter key?
	inlineCacheLock.Unlock()
	if t == nil {
		var err error
		t, err = ParseStandAlone("__inline__", text)
		if err != nil {
			return "", err
		}
		inlineCacheLock.Lock()
		inlineCache[text] = t
		inlineCacheLock.Unlock()
	}

	var b strings.Builder
	err := t.Execute(&b, data)
	return template.HTML(b.String()), err
}

var inlineCache = make(map[string]*template.Template)
var inlineCacheLock sync.Mutex

// ParseStandAlone parses the given template text
// separately from any collection of templates,
// but with this packages function definitions.
func ParseStandAlone(name, text string) (*template.Template, error) {
	return template.New(name).Funcs(FuncMap).Parse(text)
}
