package templates

import (
	"os"
	"reflect"
	"strings"
	"testing"
	"time"
)

func TestCommas(t *testing.T) {
	ts := []struct {
		input  interface{}
		output string
	}{
		{"", ""},
		{"no number", "no number"},
		{-12345.6789, "-12,345.6789"},
		{1234, "1234"},
		{12345, "12,345"},
		{".12345 inches", ".12345 inches"},
		{"12345 inches", "12,345 inches"},
		{"$123", "$123"},
		{"$1234", "$1234"},
		{"$12345", "$12,345"},
		{"($123)", "($123)"},
		{"($1234)", "($1234)"},
		{"($12345)", "($12,345)"},
		{"($12,345)", "($12,345)"}, // idempotent
		{"($12345.67) Net Income", "($12,345.67) Net Income"},
	}
	for i := range ts {
		real := Commas(ts[i].input)
		if real != ts[i].output {
			t.Errorf("Unexpected output for Commas(%v), got %q expecting %q",
				ts[i].input, real, ts[i].output)
		}
	}
}

func ExampleIndexedItem_ListSeparator() {
	seqs := [][]string{
		{"my echo", "my shadow", "me"},
		{"apples", "pears"},
		{"just me"},
	}
	t := MustParse("listSeparator", "{{range IndexedItems .}}{{.Item}}{{.ListSeparator}}{{end}}\n")
	for i := range seqs {
		t.Execute(os.Stdout, seqs[i])
	}
	// Output:
	// my echo, my shadow, and me
	// apples and pears
	// just me
}

func TestMakeSlice(t *testing.T) {
	type args struct {
		val []interface{}
	}
	tests := []struct {
		name string
		args args
		want []interface{}
	}{
		{
			name: "test list of ints",
			args: args{[]interface{}{1, 2, 3}},
			want: []interface{}{1, 2, 3},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MakeSlice(tt.args.val...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MakeSlice() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsEmpty(t *testing.T) {
	type args struct {
		val interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "nil", args: args{nil}, want: true},
		{name: "zero", args: args{0}, want: true},
		{name: "empty string", args: args{""}, want: true},
		{name: "empty struct", args: args{struct{ name string }{}}, want: true},
		{name: "empty map", args: args{map[string]int{}}, want: true},
		{name: "empty slice", args: args{[]int{}}, want: true},
		{name: "empty slice ptr", args: args{&[]int{}}, want: true},
		{name: "int val", args: args{1}, want: false},
		{name: "string val", args: args{"J.C."}, want: false},
		{name: "struct val", args: args{struct{ name string }{"lala"}}, want: false},
		{name: "map val", args: args{map[int]int{1: 1}}, want: false},
		{name: "slice", args: args{[]int{1}}, want: false},
		{name: " slice ptr", args: args{&[]int{1}}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsEmpty(tt.args.val); got != tt.want {
				t.Errorf("IsEmpty(%v): got - %v, want - %v", tt.args.val, got, tt.want)
			}
		})
	}
}

func TestIntVar(t *testing.T) {
	t.Run("set", func(t *testing.T) {
		retv := IntVar("test1", "Set", 1)
		sVal, _ := intVarStore.Load("test1")
		if sVal.(int) != 1 {
			t.Fatalf("set failed want:%d. got:%d", 1, sVal.(int))
		}
		if retv != 1 {
			t.Fatalf("set - invalid return want:%d. got:%d", 1, retv)
		}
	})

	t.Run("inc", func(t *testing.T) {

		retv := IntVar("test1", "inc", 2)
		sVal, _ := intVarStore.Load("test1")
		if sVal.(int) != 3 {
			t.Fatalf("inc failed want:%d. got:%d", 3, retv)
		}
		if retv != 3 {
			t.Fatalf("inc - invalid return want:%d. got:%d", 3, retv)
		}
	})

	t.Run("del", func(t *testing.T) {
		_ = IntVar("test1", "Set", 122)
		_ = IntVar("test1", "del")
		retv, ok := intVarStore.Load("test1")
		if ok {
			t.Fatalf("del failed want:%v. got:%v", false, retv)
		}

		if retv != nil {
			t.Fatalf("del - invalid return want:nil. got:%d", retv.(int))
		}
	})

}

func TestFormatPhoneNumber(t *testing.T) {
	tests := []struct {
		name   string
		number string
		out    string
	}{
		{name: "test 1", number: "6667771234", out: "666-777-1234"},
		{name: "test 2", number: "666777123", out: "666777123"},
		{name: "test 3", number: "+16667771234", out: "+1 666-777-1234"},
		{name: "test 4", number: "+1666777123", out: "+1666777123"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fn := FormatPhoneNumber(tt.number)
			if tt.out != fn {
				t.Error("want:", tt.out, " got:", fn)
			}
		})
	}
}

func TestMap(t *testing.T) {
	tests := []struct {
		name   string
		args   []interface{}
		key    string
		val    interface{}
		hasErr bool
	}{
		{name: "valid", args: []interface{}{"name", "ayo", "age", 21}, key: "age", val: 21},
		{name: "invalid", args: []interface{}{"name", "ayo", "age", 2}, key: "age", val: 21, hasErr: true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			retv := Map(tt.args...)
			if (retv[tt.key] != tt.val) == !tt.hasErr {
				t.Error("got wrong value ", tt.val, " for key:", tt.key)
			}
		})
	}
}

func TestIsPast(t *testing.T) {

	future, err := time.Parse("2006-01-02", "2223-02-22")
	if err != nil {
		t.Fatal(err)
	}
	past, err := time.Parse("2006-01-02", "2020-02-22")
	if err != nil {
		t.Fatal(err)
	}

	tests := []struct {
		name string
		at   time.Time
		want bool
	}{
		{
			name: "future time",
			at:   future,
			want: false,
		},
		{
			name: "past time",
			at:   past,
			want: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsPast(tt.at); got != tt.want {
				t.Errorf("IsPast() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSanitized(t *testing.T) {
	htm := `<script>alert('hello')</script><button onclick="this.preventDefault();">Click!</button>	
<h1 class="bg-red" onclick="this.preventDefault();">Hello <b>World</b></h1>`
	out := Sanitized(htm)

	if strings.Replace(string(out), "\n", "", -1) != "Click!\t<h1>Hello <b>World</b></h1>" {
		t.Log(strings.Replace(string(out), "\n", "", -1))
		t.Fail()
	}
}

func TestContainsTags(t *testing.T) {
	if ContainsTags(`lorem ipsum`) {
		t.Error(`"lorem ipsum" does not contain tags`)
	}
	if ContainsTags(`lorem ipsum<>`) {
		t.Error(`"lorem ipsum<>" does not contain tags`)
	}
	if !ContainsTags(`lorem <i>psum`) {
		t.Error(`"lorem <i>psum" contains tags`)
	}
}

func TestAdd(t *testing.T) {
	retv := Add(1, 2, 3)
	if retv != 6 {
		t.Error("want: 6, got:", retv)
	}
}

func TestSub(t *testing.T) {
	retv := Sub(1, 2, 3)
	if retv != -4 {
		t.Error("want: 6, got:", retv)
	}

	retv = Sub(2, 3)
	if retv != -1 {
		t.Error("want: -1, got:", retv)
	}
}

func TestInListString(t *testing.T) {

	tests := []struct {
		name string
		val  string
		list []string
		want bool
	}{
		{
			name: "in list",
			val:  "c",
			list: []string{"a", "b", "c"},
			want: true,
		},
		{
			name: "not in list",
			val:  "e",
			list: []string{"a", "b", "c"},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InList(tt.val, tt.list...); got != tt.want {
				t.Errorf("InList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInThisWeek(t *testing.T) {
	now := time.Date(2023, 9, 21, 16, 4, 0, 0, time.UTC)
	today := time.Date(2023, 9, 20, 16, 4, 0, 0, time.UTC)
	lastWeek := time.Date(2023, 9, 13, 16, 4, 0, 0, time.UTC)
	nextWeek := time.Date(2023, 9, 28, 16, 4, 0, 0, time.UTC)

	if !InThisWeek(today, now) {
		t.Errorf("InThisWeek(%s, %s) should be true", today, now)
	}

	if InThisWeek(lastWeek, now) {
		t.Errorf("InThisWeek(%s, %s) should be false", lastWeek, now)
	}

	if InThisWeek(nextWeek, now) {
		t.Errorf("InThisWeek(%s, %s) should be false", nextWeek, now)
	}
}
