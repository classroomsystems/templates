// Package templates provides some convenience functions
// for html/template as well as a set of functions for
// use in templates.
//
// Many of the template functions are unashamedly specific
// to US English. This package is only semi-public and
// makes no guarantees of API stability.
package templates

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"io/fs"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"strings"
	"sync"
	"time"

	"bitbucket.org/classroomsystems/ratadie"
	"github.com/microcosm-cc/bluemonday"
	"github.com/sanity-io/litter"
)

// Templates is the default template set used by this
// package.
var Templates = template.New("blank")
var FileExtension = ".tmpl"

// regex used to detect html tags
var tagRe = regexp.MustCompile(`(?m)<[\s\S]+>`)

// FuncMap is used as the initial set of functions
// available within Templates templates. It is exported
// so it may be used in other template sets.
var FuncMap = template.FuncMap{
	"Add":            Add,
	"AtFriendlyTime": AtFriendlyTime,
	"Commas":         Commas,
	"ContainsTags":   ContainsTags,
	"DaysSince":      DaysSince,
	"Debug":          Debug,
	"FriendlyPhone":  FormatPhoneNumber,
	"FriendlyTime":   FriendlyTime,
	"HTML":           HTML,
	"HasPrefix":      HasPrefix,
	"InList":         InList,
	"InThisWeek":     InThisWeek,
	"Include":        Include,
	"IndexedItems":   IndexedItems,
	// "InlineTemplate": Inline, // Added in init() to avoid initialization cycle.
	"IntRange":     IntRange,
	"IntVar":       IntVar,
	"IsEmpty":      IsEmpty,
	"IsFuture":     IsFuture,
	"IsPast":       IsPast,
	"MakeSlice":    MakeSlice,
	"Map":          Map,
	"NewSameCheck": NewSameCheck,
	"NewlineToBR":  NewlineToBR,
	"Now":          time.Now,
	"OneOf":        OneOf,
	"OnlyOne":      OnlyOne,
	"Percent":      Percent,
	"Plural":       Plural,
	"PubHref":      PubHref,
	"Quietly":      Quietly,
	"Replace":      Replace,
	"Sanitized":    Sanitized,
	"SelectOption": SelectOption,
	"Split":        Split,
	"Subtract":     Sub,
	"ToJSON":       ToJSON,
	"ToLower":      ToLower,
	"ToUpper":      ToUpper,
	"TrimSpace":    TrimSpace,
	"Widget":       Widget,
	"ZeroClass":    ZeroClass,
}

var intVarStore sync.Map

func init() {
	FuncMap["InlineTemplate"] = Inline
	Templates.Funcs(FuncMap)
}

// MustParse is a shorthand for adding a named template
// to Templates, panicking on failure.
func MustParse(name, src string) *template.Template {
	return template.Must(Templates.New(name).Parse(src))
}

// MustParseDir calls ParseDir and panics on failure.
// It returns Lookup to help with initialization. This
// way you can do the following without worrying about
// initialization order:
//
//	var tmpl = webapp.MustParseTemplateDir("templates")
//	// possibly in another file...
//	func init() {
//		DoSomething(tmpl("some-template"))
//	}
//
// In this case, Go's initialization rules guarantee that MustParseDir
// will have been called before any template lookups are performed.
func MustParseDir(dir string) func(string) *template.Template {
	err := ParseDir(dir)
	if err != nil {
		panic(err)
	}
	return Lookup
}

// ParseDir parses all the files under dir which end in
// ".tmpl" as templates. It skips files and directories
// whose name starts with ".".
//
// Templates are named based on the file's path.
// ParseDir("templates") will name the template in
// templates/a/b/c.d.tmpl "a/b/c.d".
func ParseDir(dir string) error {
	return ParseFS(os.DirFS(dir), "")
}

// MustParseFS is like ParseFS but panics on error.
func MustParseFS(fsys fs.FS, prefix string) {
	err := ParseFS(fsys, prefix)
	if err != nil {
		panic(err)
	}
}

// ParseFS parses all the files in fsys which end in
// ".tmpl" as templates. It skips files and directories
// whose name starts with "." and files whose name starts with "_".
//
// Templates are named based on the file's path and the given prefix.
// ParseFS(fsys, "foo/bar") will name the template in
// ./a/b/c.d.tmpl "foo/bar/a/b/c.d".
func ParseFS(fsys fs.FS, prefix string) error {
	return fs.WalkDir(fsys, ".", func(path string, info fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		base := info.Name()
		// Skip anything starting with ".", except the top-level folder.
		if strings.HasPrefix(base, ".") && base != "." {
			if info.IsDir() {
				return fs.SkipDir
			}
			return nil
		}
		if info.IsDir() || !strings.HasSuffix(path, FileExtension) {
			return nil
		}
		name := strings.TrimSuffix(path, FileExtension)
		if prefix != "" {
			name = prefix + "/" + name
		}
		b, err := fs.ReadFile(fsys, path)
		if err != nil {
			return err
		}
		// Can't use template.ParseFS because it would set a name we
		// don't want.
		_, err = Templates.New(name).Parse(string(b))
		if err != nil {
			return fmt.Errorf("%s(%s): %v", path, name, err)
		}
		return nil
	})
}

// Lookup is simply a shorthand for Templates.Lookup.
func Lookup(name string) *template.Template {
	return Templates.Lookup(name)
}

// Template functions below.

// PubDir is the path to a directory containing static
// files for use with PubHref.
var PubDir = "pub"

// PubHref returns a URL path starting with "/pub/"
// for the file named name. The file must exist under
// PubDir. Its current modification time is appended to
// the path as a query parameter. This enables static
// files under PubDir to be served with very long cache
// timeouts, but the query parameter will invalidate
// the cache entry whenever the file is modified.
//
// Example:
//
//	<link rel="stylesheet" href="{{PubHref "style.css"}}" />
//
// Output:
//
//	<link rel="stylesheet" href="/pub/style.css?1234567890" />
func PubHref(name string) (string, error) {
	fi, err := os.Stat(filepath.Join(PubDir, name))
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("/pub/%s?%d", filepath.ToSlash(name), fi.ModTime().Unix()), nil
}

// HTML returns the given string with its type changed
// to html/template.HTML so it will not be escaped by
// the template package. This should be used with fear
// and trembling if at all.
func HTML(s string) template.HTML {
	return template.HTML(s)
}

// Sanitized uses "bluemonday" to sanitize the content passed to it, making it safe
// to render user proved content
func Sanitized(s string) template.HTML {
	p := bluemonday.UGCPolicy()
	p.AllowAttrs("cite", "class").OnElements("blockquote")
	p.AddTargetBlankToFullyQualifiedLinks(true)
	out := p.Sanitize(s)
	return template.HTML(out)
}

// NewlineToBR returns a copy of s with all HTML
// metacharacters escaped and newlines changed to
// "<br/>". So the text may be embedded in HTML safely
// while preserving some formatting.
func NewlineToBR(s string) template.HTML {
	// s = template.HTMLEscapeString(s)
	s = string(Sanitized(s))
	s = strings.Replace(s, "\n", "<br/>", -1)
	return template.HTML(s)
}

// ToJSON returns the JSON representation of the given value.
func ToJSON(value interface{}) (template.JS, error) {
	b, err := json.Marshal(value)
	return template.JS(b), err
}

// Quietly returns the empty string, no matter what
// its argument. It is useful in templates for calling
// functions wanted only for side effects.
func Quietly(_ interface{}) string {
	return ""
}

// ToLower wraps strings.ToLower.
func ToLower(s string) string {
	return strings.ToLower(s)
}

// ToUpper wraps strings.ToUpper.
func ToUpper(s string) string {
	return strings.ToUpper(s)
}

// TrimSpace wraps strings.TrimSpace.
func TrimSpace(s string) string {
	return strings.TrimSpace(s)
}

// Percent converts a float64 ratio to a percent, that
// is 0.55 becomes 55.0.
func Percent(value float64) float64 {
	return value * 100.0
}

// OnlyOne returns nil unless its argument is a
// single-element array or slice. In that case, it
// returns the single element.
func OnlyOne(value interface{}) interface{} {
	v := reflect.ValueOf(value)
	switch v.Kind() {
	case reflect.Array, reflect.Slice:
		if v.Len() == 1 {
			return v.Index(0).Interface()
		}
	}
	return nil
}

// OneOf returns true if its first argument is reflect.DeepEqual to any of its subsequent arguments.
func OneOf(a interface{}, s ...interface{}) bool {
	for i := range s {
		if reflect.DeepEqual(a, s[i]) {
			return true
		}
	}
	return false
}

// Plural returns whether its numeric argument is plural
// according to English usage. We count all values not
// equal to 1 or -1 as requiring a plural form. Some
// English dialects use the singular for fractions less
// than 1, but we don't. E.g. "0.5 cups of water" not
// "0.5 cup of water".
func Plural(value interface{}) bool {
	switch n := value.(type) {
	case int:
		return n != 1 && n != -1
	case int8:
		return n != 1 && n != -1
	case int16:
		return n != 1 && n != -1
	case int32:
		return n != 1 && n != -1
	case int64:
		return n != 1 && n != -1
	case uint:
		return n != 1
	case uint8:
		return n != 1
	case uint16:
		return n != 1
	case uint32:
		return n != 1
	case uint64:
		return n != 1
	case float32:
		return n != 1.0 && n != -1.0
	case float64:
		return n != 1.0 && n != -1.0
	}
	return false
}

// SelectOption returns an HTML <option> tag with the
// given value. The option is selected if value and
// selectedValue have the same string representation.
// Values are converted to strings using fmt.Sprint.
func SelectOption(value, selectedValue interface{}) template.HTML {
	vs := fmt.Sprint(value)
	selected := ""
	if vs == fmt.Sprint(selectedValue) {
		selected = " selected"
	}
	return template.HTML(fmt.Sprintf("<option value=\"%s\"%s>",
		template.HTMLEscapeString(vs), selected))
}

// ZeroClass returns "zero" if fmt.Sprint(value) ==
// "0", otherwise it returns "". This is intended to
// add a class to an HTML tag to differentiate display
// of zero values. Example:
//
//	<div class="amount {{ZeroClass .}}">{{.}}</div>
func ZeroClass(value interface{}) string {
	if fmt.Sprint(value) == "0" {
		return "zero"
	}
	return ""
}

// AtFriendlyTime returns a nice time string preceded
// by "at" or "on" as appropriate.
// It assumes the location on the passed-in time is correct.
func AtFriendlyTime(value interface{}) (string, error) {
	t, ok := value.(time.Time)
	if !ok {
		return "", errors.New("FriendlyTime: Not a time value")
	}
	then := ratadie.Time(t)
	now := ratadie.Time(time.Now().In(t.Location()))
	if then == now {
		return t.Format("at 3:04pm today"), nil
	}
	if then == now-1 {
		return t.Format("at 3:04pm yesterday"), nil
	}
	if then == now+1 {
		return t.Format("at 3:04pm tomorrow"), nil
	}
	diff := then - now
	if diff < 0 {
		diff *= -1
	}
	if diff < 40 {
		return t.Format("on Jan 2"), nil
	}
	return t.Format("on Jan 2 '06"), nil
}

// FriendlyTime is just like AtFriendlyTime but without
// the "at" or "on" prefix.
func FriendlyTime(value interface{}) (string, error) {
	s, err := AtFriendlyTime(value)
	if err != nil {
		return "", err
	}
	// remove "at " or "on "
	return s[3:], nil
}

// DaysSince returns the number of calendar days that
// have passed since t. At 12:00 am, DaysSince will
// return 1 when t is the minute prior.
func DaysSince(t time.Time) int {
	then := ratadie.Time(t)
	now := ratadie.Time(time.Now())
	return int(now - then)
}

// IsFuture returns true if t is after time.Now().
func IsFuture(t time.Time) bool {
	return t.After(time.Now())
}

// IsPast returns true if t is before time.Now().
func IsPast(t time.Time) bool {
	return t.Before(time.Now())
}

// Commas converts value to a string and then adds
// commas to it as appropriate on the assumption that
// value contains a single numeric value. For example,
// -12345.6789 becomes "-12,345.6789" and "($12345.67)
// Net Income" becomes "($12,345.67) Net Income".
//
// This function does not change values that are already
// formatted correctly.
func Commas(value interface{}) string {
	commas := func(s string) string {
		ss := make([]string, 0, 1+len(s)/3)
		for len(s) > 0 {
			n := len(s) % 3
			if n == 0 {
				n = 3
			}
			ss = append(ss, s[0:n])
			s = s[n:]
		}
		return strings.Join(ss, ",")
	}
	s := fmt.Sprint(value)
	n := strings.IndexAny(s, "0123456789")
	if n < 0 {
		return s
	}
	if n > 0 && s[n-1] == '.' {
		return s
	}
	left := s[:n]
	right := strings.TrimLeft(s[n:], "0123456789")
	num := strings.TrimSuffix(s[n:], right)
	if len(num) <= 4 {
		return s
	}
	return left + commas(num) + right
}

/*
IndexedItem attaches sequence length and position
information to single items from a sequence. It
is useful for formatting particular elements of a
sequence based on their position in the sequence.

See the example for ListSeparator for a common use.
*/
type IndexedItem struct {
	Len   int
	Index int
	Item  interface{}
}

// IndexedItems creates a []IndexedItem from value. If
// value is a slice or array, then the returned slice
// contains one element for each element of the slice or
// array. Otherwise, a single-element slice is returned
// with value as the only element.
func IndexedItems(value interface{}) []IndexedItem {
	v := reflect.ValueOf(value)
	if v.Kind() != reflect.Slice && v.Kind() != reflect.Array {
		// If it's a single item, return it as a 1-item sequence.
		return []IndexedItem{{1, 0, value}}
	}
	s := make([]IndexedItem, v.Len())
	for i := range s {
		s[i] = IndexedItem{len(s), i, v.Index(i).Interface()}
	}
	return s
}

// ListSeparator returns the text that should follow
// the current item in a standard English list with
// the Oxford comma.
func (i IndexedItem) ListSeparator() string {
	if i.Len < 2 {
		return ""
	}
	if i.Index == i.Len-1 {
		return ""
	}
	if i.Len == 2 {
		return " and "
	}
	if i.Index == i.Len-2 {
		return ", and "
	}
	return ", "
}

// First returns true if i is the first item in its sequence.
func (i IndexedItem) First() bool {
	return i.Index == 0
}

// Second returns true if i is the second item in its sequence.
func (i IndexedItem) Second() bool {
	return i.Index == 1
}

// Last returns true if i is the last item in its sequence.
func (i IndexedItem) Last() bool {
	return i.Index == i.Len-1
}

// Penultimate returns true if i is the second-to-last item in its sequence.
func (i IndexedItem) Penultimate() bool {
	return i.Index == i.Len-2
}

// ModEqual returns true if i's Index is equal to r modulo d.
func (i IndexedItem) ModEqual(d, r int) bool {
	return i.Index%d == r
}

// Even returns true if i's index is even.
func (i IndexedItem) Even() bool {
	return i.ModEqual(2, 0)
}

// Odd returns true if i's index is odd.
func (i IndexedItem) Odd() bool {
	return i.ModEqual(2, 1)
}

/*
A SameCheck tracks whether a value has changed. It
can be used to remove duplicate information from a
table. Example:

	{{$name := NewSameCheck}}
	{{range .Rows}}
		<tr>
			<td>{{if $name.Remains .Name}}&rdquo;{{else}}{{.Name}}{{end}}</td>
			<td>{{.Amount}}</td>
		</tr>
	{{end}}
*/
type SameCheck struct {
	Previous interface{}
}

// Remains returns true if val is the same as the
// previous value. Val becomes the new previous value.
func (s *SameCheck) Remains(val interface{}) bool {
	r := s.Previous == val
	s.Previous = val
	return r
}

// NewSameCheck creates a new SameCheck with nil as
// the previous value.
func NewSameCheck() *SameCheck {
	return new(SameCheck)
}

// MakeSlice converts list of arguments to []interface{}
func MakeSlice(val ...interface{}) []interface{} {
	return val
}

// IsEmpty returns true if val == nil or val == zero value of its type
func IsEmpty(val interface{}) bool {
	if val == nil {
		return true
	}

	rv := reflect.ValueOf(val)
	if rv.Type().Kind() == reflect.Ptr {
		rv = rv.Elem()
	}

	if rv.IsZero() {
		return true
	}

	switch rv.Type().Kind() {
	case reflect.Map, reflect.Slice, reflect.Array:
		return rv.Len() == 0
	}

	return false
}

// IntVar key cmd [value]
/*
 commands:
	set: set the value
	del: delete the key
	inc: increment by

 Examples:
 	IntVar "key" "set" 0
 	IntVar "key" "inc" 1
	IntVar "key" "del"
*/

func IntVar(key, cmd string, v ...int) int {
	var val, retv int
	if len(v) > 0 {
		val = v[0]
	}

	cmd = strings.ToLower(cmd)

	switch cmd {
	case "set":
		intVarStore.Store(key, val)
		retv = val
	case "inc":
		kv, ok := intVarStore.Load(key)
		if !ok {
			intVarStore.Store(key, val)
			retv = val
		} else {
			retv = kv.(int) + val
			intVarStore.Store(key, retv)
		}
	case "del":
		intVarStore.Delete(key)
	}

	return retv
}

// FormatPhoneNumber friendly phone number formatting
// 6667771234 => 666-777-1234
// +16667771234 => +1 666-777-1234
// if the number has less than the required amount of digits, the number is returned verbatim
func FormatPhoneNumber(number string) string {
	retv := ""
	if len(number) < 10 {
		return number
	}

	if number[0] != '+' {
		retv = number[0:3] + "-" + number[3:6] + "-" + number[6:]
	} else {
		if len(number) < 12 {
			return number
		}

		retv = fmt.Sprint(
			number[:len(number)-10], " ", number[len(number)-10:len(number)-10+3],
			"-", number[len(number)-7:len(number)-7+3], "-", number[len(number)-4:],
		)
	}
	return retv
}

func Map(args ...interface{}) map[string]interface{} {
	retv := map[string]interface{}{}
	argLen := len(args)
	for i := 0; i < argLen; i += 2 {
		if i+1 < argLen {
			key := fmt.Sprint(args[i])
			retv[key] = args[i+1]
		}
	}

	return retv
}

// IntRange returns a list of ints intended for use with range actions where you just want to loop n times
func IntRange(limit, step int) []int {
	if step <= 0 {
		step = 1
	}
	list := make([]int, limit/step)
	for i := 0; i < limit/step; i++ {
		list[i] = i * step
	}

	return list
}

// Include renders a template in - place.
// its purpose is for situations where you need to render a template fragment in place based on a variable,
// a feature with html/template doesn't support
func Include(tplName string, ctx interface{}) template.HTML {
	tpl := Lookup(tplName)
	if tpl == nil {
		return ""
	}

	buff := bytes.NewBufferString("")
	if err := tpl.Execute(buff, ctx); err != nil {
		return ""
	}

	return template.HTML(buff.String())
}

func ContainsTags(s string) bool {
	return tagRe.Match([]byte(s))
}

// Split splits a string. (calls strings.Split)
func Split(str, sep string) []string {
	return strings.Split(str, sep)
}

// HasPrefix calls strings.HasPrefix
func HasPrefix(str, prefix string) bool {
	return strings.HasPrefix(str, prefix)
}

// Replace calls strings.Replace
func Replace(str, old, new string, n int) string {
	return strings.Replace(str, old, new, n)
}

// Debug (calls log.Println)
func Debug(arg ...interface{}) template.HTML {
	litter.Dump(arg...)
	return ""
}

func Add(v ...int) int {
	var t int
	for _, i := range v {
		t += i
	}
	return t
}
func Sub(v ...int) int {
	var t int
	if len(v) > 0 {
		t = v[0]
	}
	for _, i := range v[1:] {
		t -= i
	}
	return t
}

// InList returns true if val in []list
func InList(val string, list ...string) bool {
	for _, i := range list {
		if i == val {
			return true
		}
	}
	return false
}
func InThisWeek(t, today time.Time) bool {
	_, thisWeek := today.ISOWeek()
	_, tWeek := t.ISOWeek()

	return thisWeek == tWeek && today.Year() == t.Year()
}
