module bitbucket.org/classroomsystems/templates

go 1.18

require (
	bitbucket.org/classroomsystems/ratadie v1.0.0
	github.com/microcosm-cc/bluemonday v1.0.18
	github.com/sanity-io/litter v1.5.5
)

require (
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/gorilla/css v1.0.0 // indirect
	golang.org/x/net v0.0.0-20220622184535-263ec571b305 // indirect
)
